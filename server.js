// Import external Libs
const express = require('express')
const morgan = require('morgan')
const cors = require('cors')
// const mongoose = require('mongoose')

// Import local Libs
//const productRoutes = require('./api/routes/productsRoutes.js')
const membersRoutes = require('./api/routes/membersRoutes.js')
const companyRoutes = require('./api/routes/companyRoutes')

// App
const app = express()

// Middleware
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
let morganLogConfig = {
  default : `*DATE TIME: :date[web] | *METHOD: :method  | *URL: :url | *URL STATUS: :status | *RESPONSE TIME: :response-time ms | *CONTENT LENGTH: :res[content-length]`,
  dev: 'dev',
  tiny: 'tiny',
  short: 'short',
  common: 'common',
  combined: 'combined'
}
app.use(morgan(morganLogConfig.default)) // Middleware for http requets logging
const corsOptions = {
  origin: '*',
  allowedHeaders: ['Content-Type', 'Authorization', 'Origin', 'X-Requested-With', 'Accept'],
  'methods': ['GET', 'HEAD', 'PUT', 'PATCH', 'POST', ' DELETE'],
  preflightContinue: false,
}
app.use(cors(corsOptions))

// Routes
//app.use('/products', productRoutes)
app.use('/members', membersRoutes)
app.use('/company', companyRoutes)
app.use(express.static('public')) //Landing Page
//app.use('/', landingPageRoute)

// All other routes
app.use((req, res, next) => {
  const error = new Error('Route not found!')
  error.status = 404
  next(error)
})
app.use((error, req, res, next) => {
  res.status(error.status || 500)
  res.json({
    error: 'Something went wrong',
    'error msg': error.message || 'Server not found!',
    'error status': error.status || 500
  })
})
// Run server
const port = process.env.port || process.env.NODE_ENV_PORT || 5051
app.listen(port, () => {
  // eslint-disable-next-line no-console
  console.log(`Server up and running on port: ${port}`)
})