var admin = require("firebase-admin")
var serviceAccount = require("./serviceAccountKey.json")

//Firestore doc ref location
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://expressapi-247fd.firebaseio.com"
})
class FirestoreDB {
  constructor(collectionName){
    this.collectionName = collectionName
  }
  //Firestore ref
  docRef(){
    const db = admin.firestore()
    return db.collection(this.collectionName)
  }
  //Get data snapshot from Firestore
  async getSnapShotData(uniqueId){
    let readObj = []
    return await this.docRef().get()
    .then(snapshot => {
      snapshot.forEach(doc => {
        //console.log(`This is the uniqueId from DB: ${doc.data().uId} and the reference unique ID is ${uniqueId}`)
        if(doc.data().uId == uniqueId){
          readObj.push({[doc.id]: doc.data()})
        }
        else if(uniqueId === 'null'){
          readObj.push({[doc.id]: doc.data()})
        }
        //console.log(`This is forEach output: `, {[doc.id]: doc.data()})
      })
      return readObj
    })
    .catch(err => console.log(`Error getting documents: ${err}`))
  }
  //Find doc in Firestore
  async firestoreHasDoc(checkData){
    let resultBool
    let snapshotData
    await this.docRef().where(checkData.key, '==', checkData.value).get()
    .then(snapshot => {
      if (snapshot.empty) {
        resultBool = false
      }
      else {
        resultBool = true
      }
      snapshotData = snapshot
    })
    return {boolStatus: resultBool, databaseData: snapshotData}
    .catch(err => console.log(`Something went wrong! Error: `, err))
  }
  //Get document ref ids from Firestore
  async getDocumentRefIds(uniqueId){
    return await this.getSnapShotData(uniqueId)
    .then(dataDocs => {
      //let documentRefIds = []
      return {
        documentRefs: dataDocs.map((doc, index) => {
          return Object.keys(dataDocs[index])
          })
      }
    })
    .catch(err => console.log(`Something went wrong! Error: `, err))
  }
  //Write a document to firebase
  async writeDocument(firestoreData, checkData){
    return await this.firestoreHasDoc(checkData)
    .then((result) =>{
      let dataDocs = result.databaseData
      if(!result.boolStatus){
        this.docRef().doc()
        .set(firestoreData)
        console.log(`Records found in database collection: *${this.collectionName}* with uID: *${checkData.value}* : "${result.databaseData.size}"`)
        console.log(`Doc already exists: ${result.boolStatus}`)
        console.log(`New doc added: ${!result.boolStatus}`)
        return {
          status: 201,
          msg: `Data accetped!`,
          payload: dataDocs
        }
      }
      else {
        console.log(`Records found in database collection:" *${this.collectionName}* with uId: *${checkData.value}* : "${result.databaseData.size}"`)
        console.log(`Doc already exists: ${result.boolStatus}`)
        console.log(`Doc added: ${!result.boolStatus}`)
        return {
          status: 400,
          msg: `Data not accepted!`,
          extraMsg: 'Data not accepted because it is already saved in Firestore DB!',
          payload: dataDocs
        }
      }
    })
    .catch(err => {
      console.log(`Error getting snapshot documents: ${err}`)
      return {
        status: 400,
        msg: `Data not accepted!`,
        payload: dataDocs
      }
    })
  }
  //Patch a document to firebase
  async patchDocument(firestoreData, uniqueId){
    let patchRef = this.docRef()
    return this.getDocumentRefIds(uniqueId)
    .then(data => {
      let documentRefs = data.documentRefs
      if (documentRefs.length === 1){
        //Patch document
        documentRefs.forEach(docRefId => {
          patchRef.doc(String(docRefId)).update(firestoreData)
            console.log(`Successfully patched document: *${docRefId}* with uId: *${uniqueId}* found in Firebase collection: *${this.collectionName}*.`)
        })
        return {
          status: 201,
          msg: `Patch accetped!`,
          payload: documentRefs
        }
      }
      //Can not patch multiple documents
      else if (documentRefs.length > 1){
        documentRefs.forEach(docRefId => {
          patchRef.doc(String(docRefId)).update(firestoreData)
            console.log(`Successfully patched document: *${docRefId}* with uId: *${uniqueId}* found in Firebase collection: *${this.collectionName}*.`)
        })
        return {
          status: 400,
          msg: `Patch not accepted!`,
          extraMsg: `Patch not accepted because firebase collection: *${this.collectionName}* has multiple docs with uId: ${uniqueId}!`,
          payload: documentRefs
        }
      }
    })
    .catch(err => {
      console.log(`Error getting snapshot documents: ${err}`)
      return {
        status: 400,
        msg: `atch not accepted!`,
        payload: documentRefs
      }
    })
  }
  //Batch write documents to firestore
  async batchWrite(firestoreData){
    let result
    //No check required because this is a blind bulk write to db.
    if(firestoreData.length){
      result = false
      let batchRef = this.docRef()
      firestoreData.forEach(data => {
        batchRef.add(data)
        console.log(`This data was added to Collection *${this.collectionName}* : `, data)
      })
      console.log(`New docs added: ${!result.boolStatus}`)
      return {
        status: 201,
        msg: `Data accetped!`,
        payload: firestoreData
      }
    }
    else {
      result = true
      console.log(`Doc added: ${!result.boolStatus}`)
      return {
        status: 400,
        msg: `Data not accepted!`,
        extraMsg: 'Data not accepted because it is already saved in Firestore DB!',
        payload: firestoreData
      }
    }
  }
  //Read from Firestore
  async readDocument(uniqueId){
    return this.getSnapShotData(uniqueId)
    .then(dataDocs => {
      if (dataDocs.length === 1){
        console.log(`Found a document with uId: *${uniqueId}* from collection ${this.collectionName}: `, dataDocs)
        return {
          status: 200,
          msg: `Data accetped!`,
          payload: dataDocs
        }
      }
      else if (dataDocs.length > 1){
        console.log(`Found these documents from collection ${this.collectionName}: `, dataDocs)
        return {
          status: 200,
          msg: `Data accetped!`,
          payload: dataDocs
        }
      }
      else {
        console.log(`Firebase collection: *${this.collectionName}* has NO documents!`)
        return {
          status: 400,
          msg: `Data accepted!`,
          payload: dataDocs
        }
      }
    })
    .catch(err => {
      console.log(`Error getting snapshot documents: ${err}`)
      return {
        status: 400,
        msg: `Read not accepted!`,
        payload: dataDocs
      }
    })
  }
  //Delete from Firestore
  async deleteDocument(uniqueId){
    let patchRef = this.docRef()
    return await this.getDocumentRefIds(uniqueId)
    .then(data => {
      let documentRefs = data.documentRefs
      if (documentRefs.length === 1){
        //Delete document with uId
        documentRefs.forEach(docRefId => {
          console.log(`Document: *${docRefId}* has been successfully deleted from Firebase collection`)
          patchRef.doc(String(docRefId)).delete()
        })
          return {
            status: 200,
            msg: `Data deleted!`,
            payload: dataDocs
          }
      }
      else if (documentRefs.length > 1){
        //Delete all documents
        documentRefs.forEach(docRefId => {
          console.log(`Document: *${docRefId}* has been successfully deleted from Firebase collection`)
          patchRef.doc(String(docRefId)).delete()
        })
        return {
            status: 200,
            msg: `Data deleted!`,
            payload: dataDocs
          }
      }
      else {
        console.log(`No documents to delete from Firebase collection`)
        return {
          status: 400,
          msg: `Data not deleted!`,
          payload: documentRefs
        }
      }
    })
    .catch(err => {
      console.log(`Error getting snapshot documents: ${err}`)
      return {
        status: 400,
        msg: `Data not deleted!`,
        payload: documentRefs
      }
    })
  }
}

module.exports = FirestoreDB