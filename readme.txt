//DOCKER HELP CMDS: https://gist.github.com/bradtraversy/89fad226dc058a41b596d586022a9bd3 
// Run Apps using Docker Containers
// NodeJs Container has nodemon running to keep the Container alive else it will shut down.
// MongoDB Container is setup too provide data persistence.
// You can add many images as Micro-services

//Using Docker
1. Create a docker-compose ymal file.
    Example docker-compose file:

    version: "3"
    services:
        nodejs:
            image: node:lts
            container_name: tims-node-lts
            working_dir: /home/node/app/ExpressAPI/
            environment:
                - NODE_ENV=production
            volumes:
                - C:/Development/dockerVolumes/nodeLts/ExpressAPI/:/home/node/app/ExpressAPI/
            #expose:
            #    - 8081
            ports:
                - 8081:8081
            entrypoint: bash -c "npm init -y && npm install --save-dev nodemon && npm install express body-parser cors firebase-admin mongoose morgan joi && npx nodemon --delay 5000 --watch ./"
            #command: "npm start"
        mongo:
            image: mongo:4.0.6
            container_name: tims-mongo-406
            working_dir: /etc/mongo
            restart: always
            environment:
            # This is not secure!
            # - MONGO_INITDB_ROOT_USERNAME=admin
            # - MONGO_INITDB_ROOT_PASSWORD=admin123
              - MONGO_ENV_PORT=27017
            volumes:
                - C:/dockerVolumes/mongo406:/etc/mongo
            ports:
                - 27017:27017

2. Spin-up your apps using your docker-compose file by running this cmd:
    "docker-compose up"
3. Run cmds inside nodeJS container using ssh:
    "docker container exec it <container-name> <bash>"
    or for Win OS use:
    "winpty docker container exec -it <container-name> <bash>"
4. Run js file inside nodeJS container using ssh:
    "docker container exec -it <container-name> <./app.js>"
    or for Win OS use:
    "winpty docker container exec -it <container-name> <./app.js>"
5. Bash into nodejs container and run nodemon
    "npx modemon server.js"
6. Bash into nodejs container and install these modules:
    body-parser − This is a node.js middleware for handling JSON, Raw, Text and URL encoded form data.
    cookie-parser − Parse Cookie header and populate req.cookies with an object keyed by the cookie names.
    multer − This is a node.js middleware for handling multipart/form-data.
    morgan - Error handling module.
    mongodb - Connect to mongo DB //Optional
    mongoose - Connect to mongo DB
    cors - Configure CORS
    joi - Form input validation
