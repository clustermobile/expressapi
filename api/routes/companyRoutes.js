const express = require('express')
const Firestore = require('../../firebaseFirestore')
const router = express.Router()

let collectionName = 'company'

async function getCollectionName(req) {
  return (
    req.originalUrl.split('/').reverse()[0]
    || req.originalUrl.split('/').reverse()[1]
  )
}
// Write data from POST into Firestore DB
async function writeDataToFirestore(req, collectionName) {
  let data
  const noData = {
    uId: 'noData',
    uIdValue: 'noData',
    timeStamp: new Date(),
  }
  const keyCheck = {
    key: 'uId',
    value: 'noData',
  }
  const acceptableKeys = {
    uIdKey: 'uId',
    firstNameKey: 'firstName',
    lastNameKey: 'lastName',
  }
  //Check for default keys 
  if (Object.keys(req.body).length) {
    data = req.body
    const key = Object.keys(data).find(k => k === acceptableKeys.uIdKey)
    keyCheck.value = data[key]
    keyCheck.key = key
  } 
  else {
    console.log('There is NO data', req.body)
    data = noData
  }
  const writeDataApp = new Firestore(collectionName)
  return writeDataApp.writeDocument(data, keyCheck)
    .then(status => status)
}
//Patch data in Firestore DB
async function patchDataInFirestoreDB(req, collectionName, uniqueId = 'null') {
  let data
  const noData = {
    uId: 'noData',
    uIdValue: 'noData',
    timeStamp: new Date(),
  }
  const keyCheck = {
    key: 'uId',
    value: 'noData',
  }
  const acceptableKeys = {
    uIdKey: 'uId',
    firstNameKey: 'firstName',
    lastNameKey: 'lastName',
  }
  //Check for default keys 
  if (Object.keys(req.body).length) {
    data = req.body
    const key = Object.keys(data).find(k => k === acceptableKeys.uIdKey)
    keyCheck.value = data[key]
    keyCheck.key = key
  } 
  else {
    console.log('There is NO data', req.body)
    data = noData
  }
  const patchDataApp = new Firestore(collectionName)
  return patchDataApp.patchDocument(data, uniqueId)
    .then(status => status)
}
//Write Batch documents into Firestore DB
async function writeBatchDataToFirestore(req, collectionName) {
  let data
  const noData = {
    uId: 'noData',
    uIdValue: 'noData',
    timeStamp: new Date(),
  }
  const keyCheck = {
    key: 'uId',
    value: 'noData',
  }
  const acceptableKeys = {
    uIdKey: 'uId',
    firstNameKey: 'firstName',
    lastNameKey: 'lastName',
  }
  //Check Array is not empty 
  if (req.body.length) {
    data = req.body
  }
  else {
    console.log('There is NO data', req.body)
    data = noData
  }
  const writeBatchDataApp = new Firestore(collectionName)
  return writeBatchDataApp.batchWrite(data)
    .then(status => status)
}
// Read data from Firestore DB
async function readDataFromFirestoreDB(collectionName, uniqueId = 'null') {
  const readDataApp = new Firestore(collectionName)
  return readDataApp.readDocument(uniqueId)
}
// Delete data from Firestore DB
async function deleteDataFromFirestoreDB(collectionName, uniqueId = 'null') {
  const deleteDataApp = new Firestore(collectionName)
  return deleteDataApp.deleteDocument(uniqueId)
}
// Get all members
router.get('/', (req, res) => {
  readDataFromFirestoreDB(collectionName) // Write data to DB.
    .then(readData => {
      res.status(readData.status)
        .json({
          message: readData.msg,
          headers: req.headers,
          hostname: req.hostname,
          'Get All Documents Req': readData.payload || 'Nothing to print out!',
          'full url': `${req.protocol}://${req.get('host')}${req.originalUrl}`
        })
    })
})
// Get member by uId
router.get('/search', (req, res) => {
  const uniqueId = req.query.uid
  readDataFromFirestoreDB(collectionName, uniqueId) // Write data to DB.
    .then(readData => {
      res.status(readData.status)
        .json({
          message: readData.msg,
          headers: req.headers,
          hostname: req.hostname,
          'Document Search Req': readData.payload || 'Nothing to print out!',
          'full url': `${req.protocol}://${req.get('host')}${req.originalUrl}`
        })
    })
})
// Patch member by uId
router.patch('/search', (req, res) => {
  const uniqueId = req.query.uid
  patchDataInFirestoreDB(req, collectionName, uniqueId) // Write data to DB.
  .then(patchData => {
    res.status(patchData.status)
      .json({
        message: patchData.msg,
        headers: req.headers,
        hostname: req.hostname,
        'Patch Req': patchData.payload,
        'full url': `${req.protocol}://${req.get('host')}${req.originalUrl}`
      })
    })
})
// Delete memmber by uId
router.delete('/search', (req, res) => {
  const uniqueId = req.query.uid
  deleteDataFromFirestoreDB(collectionName, uniqueId) // Write data to DB.
  .then(deleteData => {
    res.status(deleteData.status)
      .json({
        message: deleteData.msg,
        headers: req.headers,
        hostname: req.hostname,
        'Delete Req': deleteData.payload || 'Nothing to print out!',
        'full url': `${req.protocol}://${req.get('host')}${req.originalUrl}`
      })
    })
})
// Delete all members
router.delete('/', (req, res) => {
  deleteDataFromFirestoreDB(collectionName) // Write data to DB.
  .then(deleteData => {
    res.status(deleteData.status)
      .json({
        message: deleteData.msg,
        headers: req.headers,
        hostname: req.hostname,
        'Bulk Delete Req': deleteData.payload || 'Nothing to print out!',
        'full url': `${req.protocol}://${req.get('host')}${req.originalUrl}`,
      })
    })
})
// Post (create) new member
router.post('/', (req, res) => {
  writeDataToFirestore(req, collectionName) // Write data to DB.
    .then(postData => {
      res.status(postData.status)
        .json({
          message: postData.msg,
          headers: req.headers,
          hostname: req.hostname,
          'Post Req': postData.payload || 'No order information added!',
          'full url': `${req.protocol}://${req.get('host')}${req.originalUrl}`,
        })
    })
})
router.post('/batch', (req, res) => {
  writeBatchDataToFirestore(req, collectionName) // Write data to DB.
    .then(postData => {
      res.status(postData.status)
        .json({
          message: postData.msg,
          headers: req.headers,
          hostname: req.hostname,
          'Patch Req': postData.payload || 'No order information added!',
          'full url': `${req.protocol}://${req.get('host')}${req.originalUrl}`
        })
    })
    .catch(error => {
      console.log(`Something when wrong. Error log: ${error}`)
    })
})

// Export module
module.exports = router