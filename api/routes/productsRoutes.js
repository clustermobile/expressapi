const express = require('express')
const Firestore = require('../../firebaseFirestore')

const router = express.Router()

let collectionName = 'products'

async function getCollectionName(req) {
  return (
    req.originalUrl.split('/').reverse()[0]
    || req.originalUrl.split('/').reverse()[1]
  )
}
// Write data from POST into Firestore DB
async function writeDataToFirestore(req, collectionName) {
  let data
  const noData = {
    uId: 'noData',
    uIdValue: 'noData',
    timeStamp: new Date(),
  }
  const keyCheck = {
    key: 'uId',
    value: 'noData',
  }
  const acceptableKeys = {
    uIdKey: 'uId',
    firstNameKey: 'firstName',
    lastNameKey: 'lastName',
  }
  //Check for default keys 
  if (Object.keys(req.body).length) {
    data = req.body
    const key = Object.keys(data).find(k => k === acceptableKeys.uIdKey)
    keyCheck.value = data[key]
    keyCheck.key = key
  } 
  else {
    console.log('There is NO data', req.body)
    data = noData
  }
  const writeDataApp = new Firestore(collectionName)
  return writeDataApp.writeDocument(data, keyCheck)
    .then(status => status)
}
// Read data from Firestore DB
async function readDataFromFirestoreDB(collectionName, uniqueId = 'null') {
  const readDataApp = new Firestore(collectionName)
  return readDataApp.readDocument(uniqueId)
}
// Get all products
router.get('/', (req, res) => {
  //let collectionName = getCollectionName(req)
  readDataFromFirestoreDB(collectionName) // Write data to DB.
    .then(readData => {
      res.status(200)
        .json({
          message: 'Wow !!! Get all products!',
          headers: req.headers,
          hostname: req.hostname,
          'product info': req.body || 'Nothing to print out!',
          'firestore data': readData,
        })
    })
})
// Get a product with #id
router.get('/search', (req, res) => {
  const uniqueId = req.query.uid
  readDataFromFirestoreDB(collectionName, uniqueId) // Write data to DB.
    .then(readData => {
      res.status(200)
        .json({
          message: 'Get a product!',
          headers: req.headers,
          hostname: req.hostname,
          'product info': req.body || 'Nothing to print out!',
          'firestore data': readData
        })
    })
})
// Patch order with #id
router.patch('/search', (req, res) => {
  const uniqueId = req.query.uid
  res.status(200)
    .json({
      message: `Product: #${uniqueId} updated!`,
      headers: req.headers,
      hostname: req.hostname,
      'product info': req.body || 'Nothing to print out!',
    })
})
// Delete order with #id
router.delete('/search', (req, res) => {
  const uniqueId = req.query.uid
  res.status(200)
    .json({
      message: `Product: #${uniqueId} deleted!`,
      headers: req.headers,
      hostname: req.hostname,
      'product info': req.body || 'Nothing to print out!',
    })
})
// Post (create) new product
router.post('/', (req, res) => {
  writeDataToFirestore(req, collectionName) // Write data to DB.
    .then((statusCode) => {
      res.status(statusCode.status)
        .json({
          message: statusCode.msg,
          headers: req.headers,
          hostname: req.hostname,
          'product info': req.body !== {} || 'No order information added!',
          'full url': `${req.protocol}://${req.get('host')}${req.originalUrl}`,
        })
    })
})
// Export module
module.exports = router